package com.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import com.product.entities.Product;
import com.product.service.ProductService;
@EnableEurekaClient
@SpringBootApplication
public class ProductApplication implements CommandLineRunner {
	
	@Autowired
	private ProductService productService;
	
	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Product product1=new Product("tv", 55, "sony tv");
//		Product product2=new Product("laptop", 70, "hp tv");
//		productService.addProduct(product1);
//		productService.addProduct(product2);
//		System.out.println("-------proudct added----------");
		
	}

}

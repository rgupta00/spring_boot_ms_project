package com.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class SendEmail {
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	public void sendEmail(String to, String body, String topic) {
		System.out.println("calling");
		SimpleMailMessage mailMessage=new SimpleMailMessage();
		mailMessage.setFrom("rgdemo123@gmail.com");
		mailMessage.setTo(to);//send to multiple peoples
		mailMessage.setSubject(topic);
		mailMessage.setText(body);
		
		javaMailSender.send(mailMessage);
		
		System.out.println("message send...");
	}
}

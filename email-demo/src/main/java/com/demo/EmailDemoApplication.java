package com.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class EmailDemoApplication{

	@Autowired
	private SendEmail sendEmail;
	public static void main(String[] args) {
		SpringApplication.run(EmailDemoApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void triggerEmail() {
	sendEmail.sendEmail("rgupta.mtech@gmail.com", "hello to u !", "test");
	System.out.println("---------");
	}
	
	

}

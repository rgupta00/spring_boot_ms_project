package com.order.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@Data
@Entity
@Table(name = "order_table")
public class Order {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private int totalPrice;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateOrder;
	private int agentId;
	private int customerId;
	private int productId;
	private int quantity;
	
	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;
	public Order(int totalPrice, int agentId, int customerId, int productId, int quantity,
			OrderStatus orderStatus) {
		super();
		this.totalPrice = totalPrice;
		this.dateOrder = new Date();
		this.agentId = agentId;
		this.customerId = customerId;
		this.productId = productId;
		this.quantity = quantity;
		this.orderStatus = orderStatus;
	}

	
}

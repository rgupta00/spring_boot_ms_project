package com.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.order.dto.Customer;
import com.order.dto.OrderRequest;
import com.order.dto.OrderResponse;
import com.order.dto.OrderStatusRequest;
import com.order.dto.OrderStatusResponse;
import com.order.dto.Product;
import com.order.entities.Order;
import com.order.entities.OrderStatus;
import com.order.service.OrderService;
@RestController
public class OrderController {

	@Autowired
	private KafkaTemplate<String, Order>kafkaTemplate;
	
	@Autowired
	private OrderService orderService;

	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping(value = "order")
	public ResponseEntity<List<Order>> getAllOrders(){
		return ResponseEntity.ok(orderService.getAll());
	}
	
	@PutMapping(value = "update")
	public ResponseEntity<OrderStatusResponse>
	updateOrderOrderStatus(@RequestBody OrderStatusRequest 
			orderStatusRequest) {	
		long orderId= orderStatusRequest.getOrderId();
		System.out.println("--------------------");
		Order order =orderService.getById(orderId);
		System.out.println(order);
		System.out.println("--------------------");
		
		String status= orderStatusRequest.getStatus();
		OrderStatus orderStatus=OrderStatus.valueOf(status.toUpperCase());
	
		order.setOrderStatus(orderStatus);
		
		Order updateObject= orderService.updateOrder(order);
		System.out.println("-------$$$$$$$-----");
		System.out.println(updateObject);
		System.out.println("--------$$$$$$$------------");
		OrderStatusResponse response=new OrderStatusResponse(orderId, status);
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	
	
	@PostMapping(value = "order")
	public ResponseEntity<OrderResponse> addOrder(@RequestBody OrderRequest orderRequest) {	
		String productUrl = "http://PRODUCT-SERVICE/product/" + orderRequest.getProductId();
		String customerUrl = "http://CUSTOMER-SERVICE/customer/" + orderRequest.getCustomerId();
		
		System.out.println(productUrl);
		Customer customer = restTemplate.getForObject(customerUrl, Customer.class);

		Product product = restTemplate.getForObject(productUrl, Product.class);
		
		Order orderSaved = orderService.addOrder(convertOrderRequestToOrderObject(orderRequest, 
				product.getProductPrice()));

		
		
		System.out.println("---------------------");
		System.out.println(product);
		System.out.println(customer);
		System.out.println("---------------------");

		System.out.println("------****************-----");
		kafkaTemplate.send("my_topic", orderSaved);
		System.out.println("-------message is send--------");
		OrderResponse response = new OrderResponse(orderSaved.getId(), orderSaved.getOrderStatus().name());
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	
	
	

	private Order convertOrderRequestToOrderObject(OrderRequest orderRequest, int price) {
		// Order order=new Order(totalPrice, agentName, customerId, productId, quantity,
		// orderStatus)
		Order order = new Order(orderRequest.getQuantity() * price, orderRequest.getAgentId(),
				orderRequest.getCustomerId(), orderRequest.getProductId(), orderRequest.getQuantity(),
				OrderStatus.PLACED);
		return order;

	}

}

package com.order.service;

import java.util.List;

import com.order.entities.Order;

public interface OrderService {
	List<Order> getAll();
	public Order getById(long id);
	public Order addOrder(Order order);
	public Order updateOrder(Order order);
}

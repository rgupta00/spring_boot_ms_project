package com.order.dto;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderStatusResponse {
	private long orderId;
	private String status;
	private Date date;
	public OrderStatusResponse(long orderId, String status) {
		this.orderId = orderId;
		this.status = status;
		this.date=new Date();
	}
	
	
}

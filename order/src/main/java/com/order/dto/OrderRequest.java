package com.order.dto;

import java.util.Date;

import com.order.entities.Order;
import com.order.entities.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Data
@AllArgsConstructor
public class OrderRequest {
	private int agentId;
	private int customerId;
	private int productId;
	private int quantity;
}

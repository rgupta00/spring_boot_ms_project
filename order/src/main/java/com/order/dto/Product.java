package com.order.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public class Product {
	private int productId;
	private String productName;
	private int productPrice;
	private String productDesc;
	public Product(String productName, int productPrice, String productDesc) {
		super();
		this.productName = productName;
		this.productPrice = productPrice;
		this.productDesc = productDesc;
	}
	
	
	
}

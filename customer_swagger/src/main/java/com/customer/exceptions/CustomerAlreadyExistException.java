package com.customer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class CustomerAlreadyExistException extends RuntimeException{

	public CustomerAlreadyExistException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	
	
}

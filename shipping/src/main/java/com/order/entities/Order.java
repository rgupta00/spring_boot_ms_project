package com.order.entities;

import java.util.Date;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@Data

public class Order {
	
	private long id;
	private int totalPrice;
	
	private Date dateOrder;
	private int agentId;
	private int customerId;
	private int productId;
	private int quantity;
	
	
	private OrderStatus orderStatus;
	public Order(int totalPrice, int agentId, int customerId, int productId, int quantity,
			OrderStatus orderStatus) {
		super();
		this.totalPrice = totalPrice;
		this.dateOrder = new Date();
		this.agentId = agentId;
		this.customerId = customerId;
		this.productId = productId;
		this.quantity = quantity;
		this.orderStatus = orderStatus;
	}

	
}

package com.demo.solid.ocp;

interface Shape{
	public double getArea();
}
class Rectangle implements Shape{
	public double getArea() {
		return 99.00;
	}
}

class Triangle implements Shape{
	public double getArea() {
		return 99.00;
	}
}

class Circle implements Shape{
	public double getArea() {
		return 99.00;
	}
}


class Square implements Shape{
	public double getArea() {
		return 99.00;
	}
}

class ShapeProcessor {
	public double processShape(Shape shape) {
		return shape.getArea();
	}
}


//
//class ShapeProcessor{
//	public void processShape(Object shape) {
//		//each time if i want to support new shape in need to change this code
//		//voilation of ocp
//		if(shape instanceof Circle) {
//			Circle circle=(Circle) shape;
//		}else if(shape instanceof Triangle) {
//			Triangle triangle=(Triangle) shape;
//		}
//		//
//	}
//}


public class DemoOCP {

	public static void main(String[] args) {
		//OCP: open close prin...
		// sw mouldle should be open for extension close for modification
		//OCP provide code flexibility?
		
	}
}

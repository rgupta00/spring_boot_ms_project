package com.demo.solid.srp.sol;

public interface BookDao {
	public void addBook(Book book);
}

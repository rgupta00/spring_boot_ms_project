package com.demo.solid.srp.lsp;
class Rectangle {
	private int l, b;

	public int getL() {
		return l;
	}

	public void setL(int l) {
		this.l = l;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public Rectangle(int l, int b) {
		this.l = l;
		this.b = b;
	}

	public Rectangle() {}
	
	public int getArea() {
		return l*b;
	}
}

//Rectangle ---- Square and reuse the logic of getArea()

//class Square extends Rectangle{
//	private int l;
//
//	public Square(int l) {
//		super(l, l);
//	}
//}
//in this case i am using compostion more safer: as per LSP
class Square {
	private Rectangle rectangle;
	public Square(int l) {
		rectangle=new Rectangle(l, l);
	}
	public int getArea() {
		return rectangle.getArea();
	}
	public void changeDimension(int changeDimension) {
		rectangle.setL(changeDimension);
		rectangle.setB(changeDimension);
	}
}

public class DemoLSP {
	public static void main(String[] args) {
		Square square=new Square(2);
		System.out.println(square.getArea());
		square.changeDimension(3);
		System.out.println(square.getArea());
		
	}

}

package com.demo.solid.srp;

import java.sql.Connection;
import java.sql.SQLException;

class Book {
	private int id;
	private String isbn;
	private String title;
	private String author;
	private double price;
	
	public Connection  getConnection(){
		// code to get jdbc connection..
		return null;
	}

	public SessionFactory  getFactory(){
		// code to get jdbc connection..
		return null;
	}
	public void addBook(Book book)throws SQLException{
		System.out.println("adding book to db using jdbc..");
	}
       public void addBookUsingHibernate(Book book)throws HibernateException{
		System.out.println("adding book to db using hibernate....");
	}
}
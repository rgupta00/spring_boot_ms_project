package com.customerapp.model.dao;
import java.util.*;
public interface CustomerDao {
	public List<Customer> getAllCustomers();
}

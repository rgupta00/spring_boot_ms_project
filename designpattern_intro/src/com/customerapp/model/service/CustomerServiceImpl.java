package com.customerapp.model.service;

import java.util.List;

import com.customerapp.model.dao.Customer;
import com.customerapp.model.dao.CustomerDao;
import com.customerapp.model.dao.CustomerDaoHibernate;
import com.customerapp.model.dao.CustomerDaoJdbc;

public class CustomerServiceImpl implements CustomerService {

	private CustomerDao customerDao;
	
	
	public CustomerServiceImpl(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}


	@Override
	public List<Customer> getAllCustomers() {
		System.out.println("CustomerServiceImpl is called...");
		return customerDao.getAllCustomers();
	}

}

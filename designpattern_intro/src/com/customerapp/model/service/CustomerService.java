package com.customerapp.model.service;

import java.util.List;

import com.customerapp.model.dao.Customer;

public interface CustomerService {
	public List<Customer> getAllCustomers();
}

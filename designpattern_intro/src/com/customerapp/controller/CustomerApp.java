package com.customerapp.controller;

import com.customerapp.model.dao.CustomerDao;
import com.customerapp.model.dao.CustomerDaoHibernate;
import com.customerapp.model.dao.CustomerDaoJdbc;
import com.customerapp.model.service.CustomerService;
import com.customerapp.model.service.CustomerServiceImpl;

public class CustomerApp {

	public static void main(String[] args) {
		CustomerDao customerDao=new CustomerDaoHibernate();
		CustomerService customerService=new CustomerServiceImpl(customerDao);
		customerService.getAllCustomers();
	}
}

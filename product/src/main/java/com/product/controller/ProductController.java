package com.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.product.entities.Product;
import com.product.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	//get all
	@GetMapping("product")
	public ResponseEntity<List<Product>>  getAll(){
		List<Product> products=productService.getAll();
		return ResponseEntity.ok(products);
	}
	
	//get by id
	@GetMapping("product/{productId}")
	public ResponseEntity<Product>  getAnProduct(@PathVariable(name = "productId")int productId){
		Product product=productService.getById(productId);
		return ResponseEntity.ok(product);
	}
	
	//delete
	@DeleteMapping("product/{productId}")
	public ResponseEntity<Void>  deleteAnProduct(@PathVariable(name = "productId")int productId){
		Product product=productService.deleteProduct(productId);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	
	//step 5: activate validation by apply @Valid before customer in post method and
	//update method
	
	//add 
	@PostMapping("product")
	public ResponseEntity<Product>  addAnProduct(@Valid  @RequestBody Product product){
		Product productAdded=productService.addProduct(product);
		return ResponseEntity.status(HttpStatus.CREATED).body(productAdded);
	}
	
	
	//update
	@PutMapping("product/{productId}")
	public ResponseEntity<Product>  updateAnProduct
	(@PathVariable(name = "productId")int productId, @Valid @RequestBody Product product){
		Product productUpdated= productService.updateProduct(productId, product);
		return ResponseEntity.status(HttpStatus.OK).body(productUpdated);
	}
}









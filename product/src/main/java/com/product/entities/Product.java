package com.product.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "product_table")
public class Product {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productId;
	private String productName;
	private int productPrice;
	private String productDesc;
	public Product(String productName, int productPrice, String productDesc) {
		super();
		this.productName = productName;
		this.productPrice = productPrice;
		this.productDesc = productDesc;
	}
	
	
	
}

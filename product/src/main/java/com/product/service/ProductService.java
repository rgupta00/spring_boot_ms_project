package com.product.service;

import java.util.*;

import com.product.entities.Product;


public interface ProductService {
	List<Product> getAll();
	public Product addProduct(Product product);
	public Product deleteProduct(int productId);
	public Product updateProduct(int productId, Product product);
	public Product getById(int productId);
	public Product findByName(String name);
}
